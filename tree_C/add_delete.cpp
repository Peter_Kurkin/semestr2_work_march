CartesianTrees* add_element(CartesianTrees* root, int element, int priority) {
    if (element < root->val && !root->left) {
        auto* newElement = new CartesianTrees(element, priority);
        root->left = newElement;
        return root;
    }
    if (element > root->val && !root->right) {
        auto *newElement = new CartesianTrees(element, priority);
        root->right = newElement;
        return root;
    }
    if (root->val == element)
        return root;
    if (root->val && element > root->val)
        add_element(root->right, element, priority);
    if (element < root->val && root->val)
        add_element(root->left, element, priority);


CartesianTrees* DeleteNode(CartesianTrees* root, int number){
    if (!root)
        return root;
    if (number == root->val) {
        CartesianTrees* replacement;
        if (!root->right)
            replacement = root->left; // если справа ничего нет, то достаточно взять самый первый узел слева
        else {
            CartesianTrees* pre_min_node = root->right; // узел который приведёт нас к минимальному
            if (!pre_min_node->left) {
                pre_min_node->left = root->left;
                replacement = pre_min_node;
            }
            else {
                CartesianTrees* min_node = pre_min_node->left; // узел который будет минимальным
                while (min_node->left) {
                    pre_min_node  = min_node;
                    min_node = pre_min_node->left;
                }
                pre_min_node->left   = min_node->right;
                min_node->left  = root->left;
                min_node->right = root->right;
                replacement = min_node;
            }
        }
        delete root;
        return replacement;
    }
    else if (number < root->val)
        root->left  = DeleteNode(root->left, number);
    else
        root->right = DeleteNode(root->right, number);
    return root;
}
