#include <cstddef>
#include <iostream>
#include <string>
#include <queue>
#include <stack>
#include <vector>
#include <cstdio>
#include <ctime>
#include <fstream>
#include <chrono>

struct CartesianTrees {
    int val{}, prior{};
    CartesianTrees * left{}, * right{};
    CartesianTrees() = default;
    CartesianTrees (int val, int prior) : val(val), prior(prior), left(nullptr), right(nullptr) { }
};


std::string getString(CartesianTrees* root) {
    std::string leftInorder = root->left != nullptr ? getString(root->left) : "";
    std::string rightInorder = root->right != nullptr ? getString(root->right) : "";
    if (leftInorder.length() > 0) {
        leftInorder = "(" + leftInorder + ") ";
    }
    if (rightInorder.length() > 0) {
        rightInorder = " (" + rightInorder + ")";
    }
    return leftInorder + std::to_string(root->val) + "{" + std::to_string(root->prior) + "}" + rightInorder;
}
bool search_element(CartesianTrees* tree, int element) {
    if (!tree) {
        return false;
    }
    if (tree->val == element) {
        return true;
    }
    if (search_element(tree->left, element) || search_element(tree->right, element)) {
        return true;
    }
    return false;
}
CartesianTrees* DeleteNode(CartesianTrees* root, int number){
    if (!root)
        return root;


    if (number == root->val) {

        CartesianTrees* replacement;

        if (!root->right)
            replacement = root->left; // если справа ничего нет, то досаточно взять самый первый узел слева

        else {

            CartesianTrees* pre_min_node = root->right; // узел который приведёт нас к минимальному

            if (!pre_min_node->left) {
                pre_min_node->left = root->left;
                replacement = pre_min_node;
            }

            else {
                CartesianTrees* min_node = pre_min_node->left; // узел который будет минимальным

                while (min_node->left) {  // ищем самый маленький узел
                    pre_min_node  = min_node;
                    min_node = pre_min_node->left;
                }
                pre_min_node->left   = min_node->right; //делаем грамотную замену
                min_node->left  = root->left;           //с nullptr не прокатит
                min_node->right = root->right;
                replacement = min_node;
            }
        }
        delete root;
        return replacement;


    } else if (number < root->val)
        root->left  = DeleteNode(root->left, number);
    else
        root->right = DeleteNode(root->right, number);


    return root;
}

CartesianTrees* add_element(CartesianTrees* root, int element, int priority) {
    if (element < root->val && !root->left) {
        auto *newElement = new CartesianTrees(element, priority);
        root->left = newElement;
        return root;
    }
    if (element > root->val && !root->right) {
        auto *newElement = new CartesianTrees(element, priority);
        root->right = newElement;
        return root;
    }
    if (root->val == element)
        return root;
    if (root->val && element > root->val)
        add_element(root->right, element, priority);
    if (element < root->val && root->val)
        add_element(root->left, element, priority);
    return nullptr;
}

bool search(CartesianTrees* root, int number) {
    if (root->val == number) {
        return true;
    }
    if (number > root->val && root->right)
        return search(root->right, number);
    else if (number < root->val && root->left)
        return search(root->left, number);
    return false;
}

template<class T>
int search_high_pririty(std::vector<T> &array) {
    int high_priority_number_index = 1;
    for (int index = 1; index < array.size(); index += 2) {
        if (array[high_priority_number_index] < array[index])
            high_priority_number_index = index;
    }
    return high_priority_number_index;
}
template<class T>
CartesianTrees* buildTree(std::vector<T> &array, int number) {
    std::queue<CartesianTrees*> currentQueue;
    CartesianTrees* currentNode = nullptr;


    int high_priority_number_index = search_high_pririty(array);
    int priorityNumber = array[high_priority_number_index];
    int currentNumber = array[high_priority_number_index - 1];
    array.erase(array.begin() + (high_priority_number_index - 1));
    array.erase(array.begin() + (high_priority_number_index - 1));

//    std::cout << "size array = " << array.size() << " ";
//    for (int i = 0; i < array.size(); i++) { std::cout << array[i] << " "; }
//    std::cout << "\n";

    auto* root = new CartesianTrees(currentNumber, priorityNumber);
    currentQueue.push(root);

    for (int i = 1; i < number / 2; i++) {

        high_priority_number_index = search_high_pririty(array);
        priorityNumber = array[high_priority_number_index];
        currentNumber = array[high_priority_number_index - 1];
        array.erase(array.begin() + (high_priority_number_index - 1));
        array.erase(array.begin() + (high_priority_number_index - 1));

//        std::cout << "size array = " << array.size() << " ";
//        for (int index = 0; index < array.size(); index++) { std::cout << array[index] << " "; }
//        std::cout << "\n";


        if (i % 2 == 1) {
            currentNode = currentQueue.front();
            currentQueue.pop();
        }
        if (currentNumber != -1) {
            auto* newElement = new CartesianTrees(currentNumber, priorityNumber);
            if (i % 2 == 1) {
                currentNode->left = newElement;
            } else {
                currentNode->right = newElement;
            }
            currentQueue.push(newElement);
        }
    }
    return root;
}
namespace ch = std::chrono;

template <typename duration = ch::seconds, typename clock = ch::high_resolution_clock>
class timer
{
    typename clock::time_point m_start, m_stop;

    typename clock::rep get_time() const
    {
        return ch::duration_cast<duration>(m_stop - m_start).count();
    }

public:
    void         start() { m_start = clock::now(); }
    const timer& stop()  { m_stop = clock::now(); return *this; }

    std::ostream& print() const
    {
        return std::cout << "Time running: [" << get_time() << "]";
    }
};
int main() {
    int num;
    std::cin >> num;
    std::vector<int> arr;
    timer<ch::milliseconds> aTimer;

    aTimer.start();

    // BuildTree

    for (int index = 0; index < num; index++) {
        int number;
        std::cin >> number;
        arr.push_back(number);
    }
    CartesianTrees* tree = buildTree(arr, num);

    std::ofstream out;
    out.open("C:\\Users\\Kurki\\C++\\semestr_work\\hello.txt");
    if (out.is_open()) {
        out << getString(tree) << "\n";
        out << "the tree is built" << "\n";
        // Start timer

//        out << search_element(tree, 2873) << "\n";
        add_element(tree, 15, 1);
//        out << getString(tree) << "\n";
//        DeleteNode(tree, 16562);
//        out << getString(tree) << "\n";

        //Stop timer
        aTimer.stop().print() << " milliseconds";
//    std::cout << getString(tree) << "\n";
    }
    return 0;
}
//32 3 9 10 9 1 8 6 7 -1 6 14 6 -1 6 -1 6 4 5 7 5 13 5 -1 5 -1 2 -1 2 8 10 -1 1
//((1{8}) 3{9} ((4{5}) 6{7} (7{5}))) 8{10} (10{9} ((13{5}) 14{6}))