from collections import deque


class TreeNode:

    def __init__(self, key, priority):
        self.left = 0
        self.right = 0
        self.key = key
        self.priority = priority


def merge(l_tree, r_tree):
    if l_tree is None:
        return r_tree
    if r_tree is None:
        return l_tree

    if l_tree.priority > r_tree.priority:
        l_tree.right = merge(mend(l_tree.right), mend(r_tree))
        return mend(l_tree)
    else:
        r_tree.left = merge(mend(l_tree), mend(r_tree.left))
        return mend(r_tree)


def mend(treap):
    if treap is None:
        return treap
    treap.size = hight(treap.left) + hight(treap.right) + 1
    return treap


def split(treap, key):
    if treap is None:
        return treap

    if key <= treap.key:
        new_tree = split(treap.left, key)
        treap.left = mend(new_tree[1])
        return mend(new_tree[0]), treap
    else:
        new_tree = split(treap.right, key)
        treap.right = mend(new_tree[0])
        return treap, mend(new_tree[1])


def insert(treap, val, priority):
    if val == -1:
        print('Inccorect key')
        return

    new_tree_up = split(treap, val)
    new_tree_low = split(new_tree_up[1], val + 1)
    if treap is None:
        return TreeNode(val, priority)
    else:
        return merge(merge(new_tree_up[0], TreeNode(val, priority)), new_tree_low[1])


def delete(treap, element):
    new_tree_up = split(treap, element)
    new_tree_low = split(new_tree_up[1], element + 1)
    return merge(new_tree_up[0], new_tree_low[1])


def hight(treap):
    if treap is None:
        return 0
    else:
        return treap.size


def tree_printer(treap):
    arr = deque()
    arr.appendleft(treap.priority)
    arr.appendleft(treap.key)
    print(f'{arr[0]}[{arr[1]}]')
    while len(arr) > 0:
        treap.key = arr[0]
        arr.popleft()
        arr.popleft()
        if treap.left:
            print(f'{treap.left.key}[{treap.left.key}]')
            arr.appendleft(treap.priority)
            arr.appendleft(treap.left.key)
        if treap.right:
            print(f'{treap.right.key}[{treap.right.priority}]')
            arr.appendleft(treap.priority)
            arr.appendleft(treap.right.key)


def main():
    inp = {}
    lst = list(map(int, input().split()))
    treap = TreeNode(lst[-2], lst[-1])
    lst.pop(lst[-1])
    lst.pop(lst[-1])

    for index in range(0, len(lst), 2):
        key = lst[index]
        value = lst[index + 1]
        inp[key] = value

    sorted_inp = {}
    sorted_keys = sorted(inp, key=inp.get)

    for i in sorted_keys:
        sorted_inp[i] = inp[i]

    for key in sorted_inp:
        insert(treap, key, sorted_inp[key])

    tree_printer(treap)


if __name__ == '__main__':
    main()

# 1 8 3 9 10 9 1 8 6 7 14 6 4 5 7 5 13 5 8 10
# ((1{8}) 3{9} ((4{5}) 6{7} (7{5}))) 8{10} (10{9} ((13{5}) 14{6}))
