# Need to install requests library
import requests
import os


def get_file_from_dropbox(link):
    req = requests.get(link)
    open('tests', 'wb').write(req.content)
    print("Files have been downloaded")


if not os.path.isdir("tests"):
    print('write the link on the tests folder')
    dropbox_link = input()
    get_file_from_dropbox(dropbox_link)


