from random import randint
import os


def gen_numbers():
    if not os.path.isdir("tests"):
        os.mkdir('tests')
    os.chdir('tests')

    for i in range(1, 76):

        elements_range = randint(100, 300000)

        if elements_range % 2 != 0:
            elements_range += 1

        amount_of_pairs = elements_range // 2

        lst = [str(elements_range)]

        file = open(f"{i}.txt", 'w')
        counter = 0
        while counter != amount_of_pairs:

            is_new_element = True
            num = randint(-1, 150000)
            priorities = randint(0, 100)

            for k in range(len(lst)):
                if lst[k] == num:
                    is_new_element = False
                    break

            if is_new_element:
                lst.append(str(num))
                lst.append(str(priorities))
                counter += 1

        file.write(' '.join(lst))
        print(f'number {i} test instance created')

    print('Generation is finished')


gen_numbers()
